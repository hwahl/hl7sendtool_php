<?php

$debug = false;
$mode = "carusnic";
$project_id = $subject_id = $session_id = "";
$projectErr = $subjectErr = $dobErr = $genderErr = $sessionErr = "";
$perf_phys = $pi = $gender = "";
$subject_dob = $study_date = $study_time = $study_datetime = $send_datetime = "";

$study_datetime = new DateTime("now", new DateTimeZone('Europe/Berlin'));
$send_datetime = new DateTime("now", new DateTimeZone('Europe/Berlin'));
$study_date = $study_datetime->format("Y-m-d");
$study_time = $study_datetime->format("H:i");

$dob_datetime = new DateTime("now", new DateTimeZone('Europe/Berlin'));

$isProduction = file_exists('production');