<section>
    <h2>Subject Information</h2>
    <div class="fancy-input">
        <input class="editme" type="text" name="project_id" id="project_id" value="<?php echo $project_id ?>"
               required
               pattern="<?php echo $formconfig["project_id"]["pattern"] ?>">
        <label for="project_id"><?php echo $formconfig["project_id"]["label"]?></label>
        <span class="explanation"><?php echo $formconfig["project_id"]["explanation"]?></span>
    </div>
    <div class="fancy-input">
        <input class="editme" type="text" name="subject_id" id="subject_id" value="<?php echo $subject_id; ?>"
               required
               pattern="<?php echo $formconfig["subject_id"]["pattern"] ?>">
        <label for="subject_id"><?php echo $formconfig["subject_id"]["label"]?></label>
        <span class="explanation"><?php echo $formconfig["subject_id"]["explanation"]?></span>
    </div>
    <div class="fancy-input">
        <input class="editme" type="date" name="subject_dob" id="subject_dob" value="<?php echo $subject_dob; ?>"
               required
               pattern="<?php echo $formconfig["subject_dob"]["pattern"] ?>">
        <label for="subject_dob"><?php echo $formconfig["subject_dob"]["label"] ?></label>
        <span class="explanation"><?php echo $formconfig["subject_dob"]["explanation"] ?></span>
    </div>
    <div class="fancy-input">
        <span id="genderinputs" <?php if (isset($gender)) echo 'class="valid"'?>>
        <input type="radio" name="gender" id="genderM" required
                <?php if (isset($gender) && $gender == "M") echo "checked"; ?>
                   value="M">Male
        <input type="radio" name="gender" id="genderF" required
                <?php if (isset($gender) && $gender == "F") echo "checked"; ?>
                   value="F">Female
        <input type="radio" name="gender" id="genderO" required
                <?php if (isset($gender) && $gender == "O") echo "checked"; ?>
                     value="O">Other
        <label for="gender">Gender</label>
        </span>
    </div>
</section>

<section>
    <h2>Session Information</h2>
    <div class="fancy-input">
        <input class="editme" type="text" name="session_id" id="session_id" value="<?php echo $session_id; ?>"
               required
               pattern="<?php echo $formconfig["session_id"]["pattern"] ?>">
        <label for="session_id"><?php echo $formconfig["session_id"]["label"] ?></label>
        <span class="explanation"><?php echo $formconfig["session_id"]["explanation"] ?></span>
    </div>
    <div class="fancy-input">
        <input class="editme" type="date" name="study_date" id="study_date" value="<?php echo $study_date; ?>"
               required
               pattern="<?php echo $formconfig["study_date"]["pattern"] ?>">
        <label for=" study_date"><?php echo $formconfig["study_date"]["label"] ?></label>
        <span class="explanation"><?php echo $formconfig["study_date"]["explanation"] ?></span>
    </div>
    <div class="fancy-input">
        <input class="editme" type="time" name="study_time" id="study_time" value="<?php echo $study_time; ?>"
               required
               pattern="<?php echo $formconfig["study_time"]["pattern"] ?>">
        <label for="study_time"><?php echo $formconfig["study_time"]["label"] ?></label>
        <span class="explanation"><?php echo $formconfig["study_time"]["explanation"] ?></span>
    </div>
</section>
<br>
<?php if (!$isProduction){
    $checked = ($debug) ? "checked" : "";
    echo "<div class='fancy-input'><input class='debug' type='checkbox' id='debug' name='debug' value=1 $checked>
           <label class='debug'>Debug on?</label></div>";
} ?>
<button type="button" disabled id="btnSubmit" >Probanden registrieren</button>
<div id="feedback" class="hideme"></div>

