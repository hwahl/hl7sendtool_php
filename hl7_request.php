<?php


require "hl7_send.php";
require "default_vals.php";

#TODO Inputs ISO-konform gestalten, dann parsen

if ($_SERVER["REQUEST_METHOD"] == "GET") {
    $project_id = $_GET["project_id"];
    $subject_id = $_GET["subject_id"];
    $subject_dob = $_GET["subject_dob"];
    $gender = $_GET["gender"];
    $session_id = $_GET["session_id"];
    $study_date = $_GET["study_date"];
    $study_time = $_GET["study_time"];
    $perf_phys = $_GET["perf_phys"];
    $pi = $_GET["pi"];
    $debug = isset($_GET["debug"]);
    $mode = isset($_GET["mode"]) ? $_GET["mode"] : "carusnic";
}

$debug = 1;

$project_id = "5000";
$subject_id = "666";
$subject_dob = "2019-10-10";
$gender = "O";
$session_id = "test";
$study_date = "2019-10-10";
$study_time = "10:00";


$study_datetime = date_create_from_format("Y-m-d H:i", $study_date . " " . $study_time);
$send_datetime = new DateTime("now", new DateTimeZone('Europe/Berlin'));
$dob_datetime = date_create_from_format("Y-m-d", $subject_dob);


$options = array(
    'project_id' => $project_id,
    'subject_id' => $subject_id,
    'dob_datetime' => $dob_datetime,
    'session_id' => $session_id,
    'gender' => $gender,
    'study_datetime' => $study_datetime,
    'send_datetime' => $send_datetime,
    'perf_phys' => $perf_phys,
    'pi' => $pi,
    'debug' => $debug,
    'mode' => $mode
);


//print_r($options);
$status = "TEST";
try {
    $status = generate_HL7_msg($options);
    echo $status['result']."\n";
    echo $status['message'];

}
catch(Exception $e){
    echo $e->getMessage();
}
