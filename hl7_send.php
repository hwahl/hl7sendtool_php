<?php

require "vendor/autoload.php";

use Aranyasen\HL7\Message;
use Aranyasen\HL7\Connection;

function generate_HL7_msg($options)
{
    // *_datetime-variables are Datetime-objects*

    try{

    $SUBJECT_DOB = $options['dob_datetime']->format("Ymd");
    $SENDDATE = $options['send_datetime']->format("YmdHis");
    $STUDYDATE = $options['study_datetime']->format("YmdHi");

    switch($options['mode']){
        case 'rlink':
            $ACCESSIONNR = "RLI-" . substr($SENDDATE, 2, -1);
            break;
        case 'nicpams':
            $ACCESSIONNR = "NIC-" . substr($SENDDATE, 2, -1);
            break;
        default:
            $ACCESSIONNR = "CNC-" . substr($SENDDATE, 2, -1);
            break;
    }


    $PERFORMING_PHYSICIAN = generate_compatible_names($options['perf_phys']);
    $PI = generate_compatible_names($options['pi']);

    $translate = array(
        "{PROJECT_ID}" => $options['project_id'], "{SUBJECT_ID}" => $options['subject_id'], "{SUBJECT_DOB}" => $SUBJECT_DOB,
        "{NIC_ID}" => $options['project_id'] ."_".$options['subject_id'],"{REQUEST_PROC}" => "MR-Forschungsstudie",
        "{SESSION_ID}" => $options['session_id'], "{SEX}" => $options['gender'], "{ACCESSIONNR}" => $ACCESSIONNR, "{SENDDATE}" => $SENDDATE,
        "{STUDYDATE}" => $STUDYDATE, "{PERFORMING_PHYSICIAN}" => $PERFORMING_PHYSICIAN, "{PI}" => $PI);

    $hl7template = file_get_contents(realpath("HL7-templates/".$options['mode'].".HL7"));
    $hl7msg = strtr($hl7template, $translate);

    if ($options['debug']) {
        $status = array('result' => 'success', 'message' => "DEBUG ON: {PROJECT_ID}, {SUBJECT_ID} - {SESSION_ID} (* {SUBJECT_DOB}) erfolgreich registriert<br>".$hl7msg  );
//        $status = array('result' => 'success', 'message' => "DEBUG ON: {PROJECT_ID}, {SUBJECT_ID} - {SESSION_ID} (* {SUBJECT_DOB}) erfolgreich registriert"  );
    } else {
//        $status = array('result' => 'success', 'message' => "DEBUG OFF: {PROJECT_ID}, {SUBJECT_ID} (* {SUBJECT_DOB}) erfolgreich registriert");
        $status = send_HL7_msg_socket($hl7msg);
    }

    $translate['{SUBJECT_DOB}'] = $options['dob_datetime']->format("d.m.Y");
    if ($status['result'] === 'success') {
        $status['message'] = strtr($status['message'], $translate);
    }

    return $status;
    }
    catch(Exception $e) {
        return $e->getMessage();
    }

}

function send_HL7_msg_socket($hl7msg)
{

    $ip = 'sv-pacs-eas';
    $port = '2575';
    $message = new Message($hl7msg); // Create a Message object from your HL7 string
    // Create a Socket and get ready to send message. Optionally add timeout in seconds as 3rd argument (default: 10 sec)
    $ack = (new Connection($ip, $port))->send($message); // Send a HL7 to remote listener
    $returnString = $ack->toString(true);
    if (strpos($returnString, 'MSH') === false) {
        echo "Failed to send HL7 to 'IP' => $ip, 'Port' => $port";
    }
    $msa = $ack->getSegmentsByName('MSA')[0];
    $ackCode = $msa->getAcknowledgementCode();
    $status = array('success' => False, 'message' => "");
    if ($ackCode[1] === 'A') {
        $status['result'] = 'success';
        $status['message'] = "{PROJECT_ID}, {SUBJECT_ID} (* {SUBJECT_DOB}) erfolgreich registriert";
    } else {
        $status['result'] = 'failure';
        $status['message'] = "Fehlermeldung: " . $msa->getTextMessage();
    }
    return $status;
}

function generate_compatible_names($name)
{
    if (is_null($name)) $name = '';
    $arr = explode(" ", $name);
    $title = $surname = $name = "";

    switch (count($arr)) {

        case 3:
            $title = $arr[0];
            $surname = $arr[1];
            $name = $arr[2];
            break;
        case 2:
            $surname = $arr[0];
            $name = $arr[1];
            break;
        case 1:
            $name = $arr[0];
            break;
        default:
            break;
    }
    return $title . "^" . $name . "^" . $surname;
}

