<?php

error_reporting(0);

require "hl7_send.php";

// initialize values
require "default_vals.php";

if ($_SERVER["REQUEST_METHOD"] == "GET") {
    $project_id = $_GET["project_id"];
    $subject_id = $_GET["subject_id"];
    $subject_dob = $_GET["subject_dob"];
    $session_id = $_GET["session_id"];
    $study_date =isset($_GET["study_date"]) ? $_GET["study_date"] : $study_date ;
    $study_time = isset($_GET["study_time"]) ? $_GET["study_time"] : $study_time;
    $perf_phys = $_GET["perf_phys"];
    $gender = $_GET["gender"];
    $pi = $_GET["pi"];
    $study_datetime = date_create_from_format("Y-m-d H:i", $study_date . " " . $study_time);
    $send_datetime = date_create();
    $dob_datetime = date_create_from_format("Y-m-d", $subject_dob);
    $debug = isset($_GET["debug"]);
    $mode = isset($_GET["mode"]) ? $_GET["mode"] : "carusnic";
}

// set default values for specific modes
switch ($mode){
    case "rlink":
        $project_id = "04";
        break;
}

// load configuration for the form depending on the mode
$formconfig = parse_ini_file(realpath("HL7-forms/".$mode.".ini"), TRUE);

// send form and generate HL7-message

if (!empty($project_id) and !empty($subject_id) and !empty($dob_datetime) and !empty($gender)
    and !empty($session_id) and !empty($study_datetime)) {

    $options = array(
        'project_id' => $project_id,
        'subject_id' => $subject_id,
        'dob_datetime' => $dob_datetime,
        'session_id' => $session_id,
        'gender' => $gender,
        'study_datetime' => $study_datetime,
        'send_datetime' => $send_datetime,
        'perf_phys' => $perf_phys,
        'pi' => $pi,
        'debug' => $debug,
        'mode' => $mode
    );

    $status = generate_HL7_msg($options);

} else {
    $status = array("result" => "hideme", "message" => "");
}
// configure feedback depending on results
switch ($status['result']) {
    case "success":
        $msg = strtr($status['message'], array("\n" => "<br />", "\r" => "")) . " um " . $send_datetime->format('d.m.Y H:i:s') ." Uhr";
        break;
    case "failure":
        $msg = "Fehler beim Senden um " . $send_datetime->format('d.m.Y H:i:s') . "<br /><samp>"
            . strtr($status['message'], array("\n" => "<br />", "\r" => ""))."</samp>";
        break;
    default:
        $msg = "";
        break;
}
?>
<!--<!DOCTYPE html>-->

<html>
<head>
    <meta charset="utf-8">
    <title>HL7 Sendtool</title>
    <link rel=icon href="gfx/Logo_Carus_sand_lores.jpg">
    <!--<link href="reset.css" rel="stylesheet" type="text/css">-->
    <link href="css/fancy_form.css?<?php echo filemtime('css/fancy_form.css') ?>" rel="stylesheet" type="text/css">
    <link href="css/form.css?<?php echo filemtime('css/form.css') ?>" rel="stylesheet" type="text/css">
    <script>
        var name = "<?php echo $status['result']?>";
        var msg = "<?php echo $msg ?>";
    </script>
    <script src="js/form.js?<?php echo filemtime('js/form.js') ?>"></script>
</head>
<body>

<form method="get" id="registrationform" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
    <div class="header"><h1>Carus<span style="color: grey;">Neuro</span>ImagingCenter <br/>Probanden Registrierung</h1>
        <?php
        echo $formconfig["logo"]["img"];
        ?>
    </div>
    <hr/>
    <input type="hidden" name="mode" id="mode" value="<?php echo $mode?>">
    <span id="switchmode"><span id="note">Selected Mode</span>
        | <a id="mode_carusnic" <?php echo ($mode == 'carusnic' ? 'class="active"' : '')?>href="index.php?mode=carusnic">CarusNIC (IMPAX-Speicherung)</a>
        | <a id="mode_nicpams" <?php echo ($mode == 'nicpams' ? 'class="active"' : '')?> href="index.php?mode=nicpams">NIC PaMS (XNAT-Speicherung)</a>
        | <a id="mode_rlink" <?php echo ($mode == 'rlink' ? 'class="active"' : '')?> href="index.php?mode=rlink">RLink</a>
        |</span>
    <hr/>
    <div id="note">
        <?php
        if ($formconfig){
            echo $formconfig["note"]["text"];
        } else{
            echo "form configuration loading failed";
        }
        ?>
    </div>
    <hr/>
    <?php
    // load form
    require "hl7_form.php";
    ?>

</form>
<!-- start fancy_form-Skript at the end otherwise it won't work properly -->
<script src="js/fancy_form.js?<?php echo filemtime('js/fancy_form.js') ?>"></script>
</body>
</html>