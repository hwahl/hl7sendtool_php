var fancyInputElements = document.querySelectorAll('.fancy-input input');
for (var i = 0; i < fancyInputElements.length; i++) {
    scaleLabel(fancyInputElements[i], true);
    fancyInputElements[i].addEventListener('focus', function () {
        scaleLabel(this, false)
    });
    fancyInputElements[i].addEventListener('blur', function () {
        scaleLabel(this, true)
    });
    fancyInputElements[i].addEventListener('change', function () {
        scaleLabel(this, false)
    });
}

function scaleLabel(element, isLikePlaceholder) {
    if (isLikePlaceholder) {
        if (element.value === '') {
            element.parentNode.querySelector('label')
                .classList.add('like-placeholder');
        }
    } else {
        element.parentNode.querySelector('label')
            .classList.remove('like-placeholder');
    }
}
