// add eventlisteners
document.addEventListener('DOMContentLoaded', function () {

    document.getElementById('btnSubmit').addEventListener('click', submitform);
    var inputs = document.querySelectorAll('input:required');
    for (let i = 0; i < inputs.length; i++) {
        inputs[i].addEventListener('change', check);
        switch (inputs[i].name) {
            case 'project_id':
                inputs[i].addEventListener('change', check_project);
                break;
            case 'subject_id':
                inputs[i].addEventListener('change', check_subject);
                break;
            case 'gender':
                inputs[i].addEventListener('change', check_gender);
                break;
            default:
                inputs[i].addEventListener('change', check);
        }
    }
    check();
    cfgFeedback();
});

// define constants
const qa_projects = ['01306', '01307', '01308'];
const qa_subjects = [
    {name: "SP5300ml", dob: '1998-01-01'},
    {name: "NICSOLID", dob: '2019-01-22'}
];


// define functions
function cfgFeedback() {
    // use global variables "name" and "msg", defined in index.php
    document.getElementById('feedback').className = name;
    document.getElementById('feedback').innerHTML = msg;
}

function check() {
    // activate submit-button if all required fields are valid
    document.getElementById('btnSubmit').disabled = !(
        document.getElementById('project_id').validity.valid &&
        document.getElementById('subject_id').validity.valid &&
        document.getElementById('subject_dob').validity.valid &&
        document.getElementById('session_id').validity.valid &&
        document.getElementById('study_date').validity.valid &&
        document.getElementById('study_time').validity.valid &&
        // document.getElementById('gender').validity.valid);
        (document.getElementById('genderF').validity.valid ||
            document.getElementById('genderM').validity.valid ||
            document.getElementById('genderO').validity.valid));
}

function submitform() {
    // submit form
    document.forms['registrationform'].submit();
}

function check_project() {
    // check if project-id is a QA-project
    if (qa_projects.indexOf(document.getElementById('project_id').value) >= 0) {
        // if mode is not 'nicpams', set mode right
        if (document.getElementById('mode').value != 'nicpams') {
            var oldmode = document.getElementById('mode').value;
            // set correct mode
            document.getElementById('mode').value = 'nicpams';
            // remove active-class from oldmode
            document.getElementById('mode_'+oldmode).classList.remove('active');
            // set correct mode as active
            document.getElementById('mode_nicpams').className = 'active';
        }
        // set default subject-id for QA
        document.getElementById('subject_id').value = 'SP5300ml';
        // check if subject is a valid QA Phantom
        check_subject();
    }
    check();
}

function check_subject() {
    // check each subject in list if it is valid, but only if valid project-ID exist
    qa_subjects.forEach(function (val) {
        if (document.getElementById('subject_id').value === val.name &&
            qa_projects.indexOf(document.getElementById('project_id').value) >= 0) {
            // set DOB
            document.getElementById('subject_dob').value = val.dob;
            // set Gender to Other
            document.getElementById('genderO').checked = true;
            check_gender();
            // update session-ID for compliance with QA-guidelines: 2019-12-01 --> QA191201
            var parts = document.getElementById('study_date').value.split('-');
            document.getElementById('session_id').value = 'QA' + parts[0].slice(-2) + parts[1] + parts[2];

            // trigger change-Event
            var event = document.createEvent("HTMLEvents");
            event.initEvent("focus",true,false);
            document.getElementById('project_id').dispatchEvent(event)
            document.getElementById('subject_id').dispatchEvent(event);
            event.initEvent("change",true,false);
            document.getElementById('subject_dob').dispatchEvent(event);
            document.getElementById('study_date').dispatchEvent(event);
            document.getElementById('study_time').dispatchEvent(event);
            document.getElementById('genderO').dispatchEvent(event);
            document.getElementById('session_id').dispatchEvent(event);
        }
    });
    check();
}

function check_gender() {
    // add class if selected
    document.getElementById('genderinputs').className = 'valid';
    check();
}
